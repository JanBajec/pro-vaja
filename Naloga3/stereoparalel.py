import numpy as np
import matplotlib.pyplot as pyplot


def izracunaj_sad(slika_leva, slika_desna, okno=11, d_max=100, smer='ld'):
    if smer == 'ld':
        c_sad, d_vals = izracunaj_sad(np.fliplr(slika_desna), np.fliplr(slika_leva), okno, d_max, 'dl')
        c_sad = np.flip(c_sad, axis=(1, 2))
        d_vals = np.flip(d_vals)*-1
    else:
        slika_leva = slika_leva.astype(np.int)
        slika_desna = slika_desna.astype(np.int)

        d_vals = np.arange(d_max)

        height, width = slika_leva.shape

        c_sad = np.zeros((height, width, d_max))
        c_sad[:, :, :] = np.inf

        #im_diff = np.abs(slika_leva - slika_desna).sum(2)

        K = np.ones((okno, okno))
        # im_diff = np.convolve(im_diff, K)

        for x in range(d_max-1):
            im_diff = np.abs(slika_desna[:, :-x] - slika_leva[:, x, :]).sum(2)
            im_diff = np.convolve(im_diff, K)
            c_sad[:, :-x, x] = im_diff

    return c_sad, d_vals

def oceni_podpikselno_dispariteto(slika_sad, d_vred):
    height, width, depth = slika_sad.shape
    depth = depth - 1
    D = (slika_sad.argmin(2))
    slika_disp_subpix = np.ndarray((height, width), dtype=np.float64)
    adj_mask = (D > 0) * (D < depth)
    for x in range(height):
        for y in range(width):
            D_plus = D[x, y] + 1
            D_minus = D[x, y] - 1
            if D_minus == -1:
                D_minus = 0
            if D_plus > depth:
                D_plus = depth
            C = (slika_sad[x, y, D[x, y]])
            C_plus = slika_sad[x, y, D_plus]
            C_minus = slika_sad[x, y, D_minus]
            slika_disp_subpix[x, y] = np.float64((d_vred[slika_sad.argmin(2)])[x][y])
            if adj_mask[x, y]:
                slika_disp_subpix[x][y] -= ((C_plus - C_minus) / (2 + (C_plus - (2 * C) + C_minus)))
    return slika_disp_subpix

def poisci_ujemajoce_disparitete(slika_disp_1, slika_disp_2):
    height, width = slika_disp_1.shape
    maska_ujemanje = np.ndarray((height, width), dtype=np.bool)
    for x in range(height):
        for y in range(width):
            d = slika_disp_1[x, y]
            tmp = slika_disp_2[x, y + d]

            if abs(d +tmp) <= 1:
                maska_ujemanje[x, y] = 1
            else:
                maska_ujemanje[x, y] = 0
    return maska_ujemanje

def izracunaj_globinsko_sliko(slika_leva, slika_desna, f, B, d_off, okno=11, d_max=100):
    pass



# slika1 = pyplot.imread('./im_test_left.png')
# slika1 = np.uint8(((slika1[::,::,0] + slika1[::,::,1] + slika1[::,::,2]) / 3) * 255)
# slika2 = pyplot.imread('./im_test_right.png')
# slika2 = np.uint8(((slika2[::,::,0] + slika2[::,::,1] + slika2[::,::,2]) / 3) * 255)
#
#
# ena, dva = izracunaj_sad(slika1, slika2)
# print(ena)
# print(dva)
