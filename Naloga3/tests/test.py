import unittest
import numpy as np

from stereoparalel import *


class TestIzracunajSad(unittest.TestCase):

    def test_2_oceni_podpikselno_dispariteto(self):
        slika_sad = np.array([
                [0, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 0],
                [1, 1, 1, 0.1, 0.8, 1],
                [1, 1, 0.3, 0, 0.7, 1],
                ]).reshape(2, 2, 6)
        d_vred1 = np.array([0, 1, 2, 3, 4, 5])
        d_vred2 = np.array([-5,-4,-3,-2,-1,0])

        disp_sp1_ref = np.array([0, 5, 3.0625, 2.8]).reshape(2, 2)
        disp_sp2_ref = np.array([-5, 0, -1.9375, -2.2]).reshape(2, 2)

        disp_sp1_est = oceni_podpikselno_dispariteto(slika_sad, d_vred1)
        disp_sp2_est = oceni_podpikselno_dispariteto(slika_sad, d_vred2)

        np.testing.assert_array_equal(disp_sp1_ref, disp_sp1_est)
        np.testing.assert_array_equal(disp_sp2_ref, disp_sp2_est)

    def test_3_poisci_ujemajoce_disparitete(self):

        disp_l = np.array([
            [0,-1,-2,-3,-4,-5, -6],
            [0,-1,-2,-3,-4,-5, -6],
            [0,-1,-2,-3,-4,-5, -6],
            [0,-1,-2,-3,-4,-5, -6],
            ])
        disp_d = np.array([
            [0, 0,  0, 0, 0, 0, 0],
            [1, 1,  1, 1, 0, 0, 0],
            [2, 2,  2, 2, 0, 0, 0],
            [3, 3,  3, 3, 0, 0, 0],
            ])
        ujemanje_ld_ref = np.array([
            [1, 1, 0, 0, 0, 0, 0],
            [1, 1, 1, 0, 0, 0, 0],
            [0, 1, 1, 1, 0, 0, 0],
            [0, 0, 1, 1, 1, 0, 0],
            ], dtype=np.bool)
        ujemanje_dl_ref = np.array([
            [1, 1, 0, 0, 0, 0, 0],
            [1, 1, 0, 0, 0, 0, 0],
            [1, 1, 0, 0, 0, 0, 0],
            [1, 1, 0, 0, 0, 0, 0],
            ], dtype=np.bool)

        ujemanje_ld_est = poisci_ujemajoce_disparitete(disp_l, disp_d)
        ujemanje_dl_est = poisci_ujemajoce_disparitete(disp_d, disp_l)

        np.testing.assert_array_equal(ujemanje_ld_ref, ujemanje_ld_est)
        np.testing.assert_array_equal(ujemanje_dl_ref, ujemanje_dl_est)



if __name__ == '__main__':
    unittest.main(verbosity=2)
